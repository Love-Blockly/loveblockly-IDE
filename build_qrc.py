import os

def build_dir(dir, f):
	for fi in os.listdir(dir):
		full = dir + "/" + fi
		if not fi.startswith("."):
			if os.path.isfile(full):
				f.write("\t<file>" + full + "</file>\n")
			else:
				build_dir(full, f)



f = open("browser.qrc","w")
f.write("<!DOCTYPE RCC><RCC version=\"1.0\"><qresource>\n")

build_dir("browser", f)

f.write("</qresource></RCC>\n")
f.close()