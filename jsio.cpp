/**
 * Copyright 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jsio.h"

#include <QDebug>
#include <QDir>
#include "runningloveinstance.h"

JSIO::JSIO(QObject *parent) : QObject(parent) {

}

/**
 * @brief JSIO::runLua Run a given piece of lua code.
 * @param lua The lua to run.
 */
void JSIO::runLua(QString lua) {
    // the path with the main.lua file
    QDir runPath;

    // make it if it doesn't exist, and cd into it.
    if(!runPath.cd(FULL_PATH)) {
        runPath.mkpath(FULL_PATH);
        runPath.cd(FULL_PATH);
    }

    // open main.lua for writing, in text mode.
    QFile main(runPath.filePath("main.lua"));
    if (!main.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    // make output stream for it
    QTextStream out(&main);

    // write the lua.
    out << lua << "\n";

    // actually flushes it, Qt automagically closes it when it
    //  goes out of scope.
    main.close();

    // start a instance of Love running, and show it's output log.
    RunningLoveInstance *w = new RunningLoveInstance();\
    w->show();
}

