/**
 * Copyright 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Runs a instance of Love, and puts it's output into a new window.
 */

#ifndef RUNNINGLOVEINSTANCE_H
#define RUNNINGLOVEINSTANCE_H

#include <QMainWindow>
#include <QProcess>

namespace Ui {
class RunningLoveInstance;
}

class RunningLoveInstance : public QMainWindow
{
    Q_OBJECT

public:
    explicit RunningLoveInstance(QWidget *parent = 0);
    ~RunningLoveInstance();

private:
    Ui::RunningLoveInstance *ui;
    /**
     * @brief process
     * The process Love is running in.
     */
    QProcess *process;

protected slots:
    /**
     * @brief readReady
     * Called when the process is ready to be read, with fresh new data->
     */
    void readReady();

    /**
     * @brief processEnded
     * The process has ended, time to close the logging window.
     */
    void processEnded();
};

#endif // RUNNINGLOVEINSTANCE_H
