/**
 * Copyright 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "runningloveinstance.h"
#include "ui_runningloveinstance.h"
#include "jsio.h"

RunningLoveInstance::RunningLoveInstance(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RunningLoveInstance)
{
    ui->setupUi(this);

    const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    ui->output->setFont(fixedFont);

    QString program = "love";
    QStringList arguments;
    arguments << PROJET_NAME;

    process = new QProcess(NULL);
    process->setWorkingDirectory(PATH_RUNTIME);
    process->start(program, arguments);

    connect(process, SIGNAL(readyReadStandardOutput()), SLOT(readReady()));
    connect(process, SIGNAL(finished(int)), SLOT(processEnded()));
}

RunningLoveInstance::~RunningLoveInstance()
{
    delete ui;
}

void RunningLoveInstance::readReady() {
    QByteArray data = process->readAllStandardOutput();
    QString str(data);
    ui->output->appendPlainText(str);
}

void RunningLoveInstance::processEnded() {
    close();
}
