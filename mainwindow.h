/**
 * Copyright 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The main window, with the embedded web browser
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "jsio.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    /**
     * @brief jsio
     * The JSIO object we expose to JS.
     */
    JSIO* jsio;

protected slots:
    /**
     * @brief setupAPI
     * Set up the Javascript interface, allowing it to talk to us.
     */
    void setupAPI();
};

#endif // MAINWINDOW_H
