/**
 * Copyright 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A class to act as a interface between C++ and JavaScript, where the rest of LoveBlockly is.
 */

#ifndef JSIO_H
#define JSIO_H

#include <QObject>

// Define the paths where things will be built
#define PATH_RUNTIME QString("tmp")
#define PROJET_NAME QString("testing")

// A shortcut for getting the full path to the project directory
#define FULL_PATH PATH_RUNTIME + QDir::separator() + PROJET_NAME

class JSIO : public QObject
{
    Q_OBJECT
public:
    explicit JSIO(QObject *parent = 0);
    /**
     * Run the specified Love program.
     */
    Q_INVOKABLE void runLua(QString);
};

#endif // JSIO_H
