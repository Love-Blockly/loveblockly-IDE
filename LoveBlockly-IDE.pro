#-------------------------------------------------
#
# Project created by QtCreator 2016-07-18T14:14:38
#
#-------------------------------------------------

QT        += core gui webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LoveBlockly-IDE
TEMPLATE = app

SOURCES  += main.cpp\
        mainwindow.cpp \
    jsio.cpp \
    runningloveinstance.cpp

HEADERS   += mainwindow.h \
    jsio.h \
    runningloveinstance.h

FORMS     += mainwindow.ui \
    runningloveinstance.ui

RESOURCES += browser.qrc
