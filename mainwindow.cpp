/**
 * Copyright 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QWebChannel>
#include <QWebEngineProfile>
#include <QWebEnginePage>
#include <QWebEngineSettings>
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), jsio(new JSIO)
{
    ui->setupUi(this);
    ui->browser->setUrl(QUrl("qrc:/browser/index.html"));

    connect(ui->browser, SIGNAL(urlChanged(QUrl)), SLOT(setupAPI()));
}

void MainWindow::setupAPI() {
    QWebEnginePage* page = ui->browser->page();
    page->runJavaScript("IDE = {};");

    QWebChannel* channel = new QWebChannel(page);
    page->setWebChannel(channel);
    channel->registerObject(QString("IDE_IO"), jsio);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete jsio;
}
